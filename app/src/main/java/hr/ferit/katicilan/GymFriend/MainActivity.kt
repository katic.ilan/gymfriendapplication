package hr.ferit.katicilan.GymFriend

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FirebaseFirestore
import hr.ferit.katicilan.GymFriend.ui.theme.GymFriendTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        setContent {
            GymFriendTheme {
                val db = FirebaseFirestore.getInstance()
                val navController = rememberNavController()
                AppNavigation(navController = navController)
                }
            }
        }
    }
