package hr.ferit.katicilan.GymFriend

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController

@Composable
fun InfoScreen(navController: NavController) {
    LazyColumn(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.Black)
            .padding(bottom = 60.dp)
    ) {
        item {
            ScreenTitle(
                title = "Info",
                subtitle = "Have a great workout"
            )
            Text(
                text = "GymFriend is app, that can help you with exercise choice " +
                        "if you are clueless and you don't know what to do today. " +
                        "Here you can find a lot of different exercise. All you must " +
                        "do is just click on your group of muscles and it will guide you " +
                        "on screen with plenty of workouts...",
                fontSize = 15.sp,
                color = Color.Gray,
                modifier = Modifier
                    .padding(10.dp)
                    .padding(bottom = 50.dp)
            )
            Text(text = "If you have some new ideas, or maybe some issues with app you can contact me",
                fontSize = 15.sp,
                color = Color.Gray,
                modifier = Modifier
                    .padding(10.dp))
                SocialLinks()
            Text(
                text = "created by:  ILAN KATIĆ",
                fontSize = 15.sp,
                color = Color.Gray,
                modifier = Modifier
                    .padding(10.dp).padding(top = 50.dp))

        }
    }
    NavigationBar(navController = navController)
}


@Composable
fun SocialLinks() {
    Row(horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.fillMaxWidth()
    ) {
        SocialLink("Facebook", "https://www.facebook.com/", R.drawable.facebook_icon)
        SocialLink("Instagram", "https://www.instagram.com/", R.drawable.instagram_icon)
        SocialLink("Twitter", "https://twitter.com/", R.drawable.twitter_logo)
    }
}

@Composable
fun SocialLink(name: String, url: String, iconResId: Int) {
    val context = LocalContext.current

    IconButton(onClick = {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(intent)
    }) {
        Image(
            painter = painterResource(id = iconResId),
            contentDescription = name,
            modifier = Modifier.size(30.dp)
        )
    }
}
