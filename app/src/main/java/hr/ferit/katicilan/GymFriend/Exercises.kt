package hr.ferit.katicilan.GymFriend

val ChestExercises = listOf(
    Exercise(R.drawable.benchpress, "Bench Press is the best workout for hitting chest muscles" +
        ". It has 2 variation with weighted bar or with dumpbells. You just lay on the bench"+
        " and try to lift the weights above your head",true),
    Exercise(R.drawable.dips, "Dips are a compound, body-weight exercise. You do Dips by first raising yourself on two dip bars with straight arms. Lower your body until your shoulders are below your elbows. Push yourself up until your arms are straight again. Dips work your chest, shoulders, back and arm muscles.",
    false),
    Exercise(R.drawable.chest, "ChestFly is great workout for chest definition, you can do it with cables " +
        "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
    false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
        " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",true))

val chest = MuscleGroup("Chest", ChestExercises)

val BackExercises = listOf(
    Exercise(R.drawable.cablerow, "Cable Row - This movement really targets the rhomboids and lats, two of the largest back muscles in the body. It build strengths that translates into loads of other exercises, such as pullups and deadlifts. Building strength through our back muscles helps us maintain proper posture and protect the spine.",false),
    Exercise(R.drawable.barbell, "Barbell row - exercise for lateral muscles and it is one of the most important workouts for back. You just need weighted bar, bend down and pull the bar to your chest.",
        false),
    Exercise(R.drawable.chest, "ChestFly is great workout for chest definition, you can do it with cables " +
            "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
        false),
    Exercise(R.drawable.back, "PullUps are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val back = MuscleGroup("Back", BackExercises)


val ArmsExercises = listOf(
    Exercise(R.drawable.tricepsex, "The triceps extension is an isolation exercise that works the muscle on the back of the upper arm.Triceps brachii hypertrophy is substantially greater after elbow extension training performed in the overhead versus neutral arm position. This muscle, called the triceps, has three heads: the long head, the lateral head, and the medial head. The three heads work together to extend the forearm at the elbow joint.",false),
    Exercise(R.drawable.arms, "Biceps Curls, most common workout that comes to mind when you hear GYM" +
            " You just need some weight no matter what it is, and you just curl it with your elbow (without shoulders)",
        false),
    Exercise(R.drawable.forearm, "Most of the people forget about this part of arms, but it gives a lot volume to it. All you need is some weight that you can fit in your hand and try to go up and down to the level you feel it burning."
        ,
        false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val arms = MuscleGroup("Arms", ArmsExercises)


val LegsExercises = listOf(
    Exercise(R.drawable.legs, "Leg extension is gym workout that requires machine. All you need to do is sit on the machine and try to lift weights with legs only with motion of knee",false),
    Exercise(R.drawable.plank, "Squats - have 2 variations of workout( weighted or bodyweight), If you go with weighted version you need a barbell and put it on your shoulder and try to squat, taking care that your knees don't go over your feet",
        false),
    Exercise(R.drawable.bulgarian, "Bulgarian split squat - is one of the most hated exercises, because of its complex. You need for this one bench and some weights, basically you put one leg on the bench and other is on the ground and you try to squat on that leg"
         ,
        false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val legs = MuscleGroup("Legs", LegsExercises)


val CardioExercises = listOf(
    Exercise(R.drawable.bicycle, "Bicycle - another great exercise for burning calories, you just ride a bike and calories goes down"
        ,false),
    Exercise(R.drawable.rowing, "Rowing - workout that burns calories very fast, and in my opinion is the best for that purpose, name says it all - you just row like in boat",
        false),
    Exercise(R.drawable.treadmill, "Treadmill - best workout for burning calories is running, and in the gym you have machine for that, so i don't have anything to say, just go there and run",
        true),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val cardio = MuscleGroup("Cardio", CardioExercises)


val BodyweightExercises = listOf(
    Exercise(R.drawable.pushup, "Push-Ups - workout that everyone can do, no matter where you are. All you need is lay down and push you up",false),
    Exercise(R.drawable.situps, "Sit-ups - best workout for abdominal muscles, and it is recomended for everyone to do it. Just lay on your back and lift your upper body from the ground",
        false),
    Exercise(R.drawable.plank1, "Plank - best workout for whole body, almost every muscle works in plank. It have a lot of variations, but all you need is just a regular one",
        false),
    Exercise(R.drawable.dips, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val bodyweight = MuscleGroup("Bodyweight", BodyweightExercises)