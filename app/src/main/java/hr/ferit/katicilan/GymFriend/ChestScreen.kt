package hr.ferit.katicilan.GymFriend


import androidx.compose.runtime.Composable
import androidx.navigation.NavController


@Composable
fun ChestScreen(navController: NavController) {
    MuscleGroupScreen(muscleGroup = chest, navController = navController)
}

@Composable
fun BackScreen(navController: NavController) {
    MuscleGroupScreen(muscleGroup = back, navController = navController)
}

@Composable
fun LegScreen(navController: NavController) {
    MuscleGroupScreen(muscleGroup = legs, navController = navController)
}
@Composable
fun CardioScreen(navController: NavController) {

    MuscleGroupScreen(muscleGroup = cardio, navController = navController)
}
@Composable
fun BodyweightScreen(navController: NavController) {
    MuscleGroupScreen(muscleGroup = bodyweight, navController = navController)
}
@Composable
fun ArmScreen(navController: NavController) {
    MuscleGroupScreen(muscleGroup = arms, navController = navController)
}
