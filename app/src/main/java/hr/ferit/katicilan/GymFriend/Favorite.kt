package hr.ferit.katicilan.GymFriend

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

val allExercises = listOf(chest, back, arms, legs, cardio, bodyweight)

@Composable
fun FavoriteScreen(navController: NavController, favoriteExercises: List<Exercise>) {
    LazyColumn(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.Black)
            .padding(bottom = 60.dp)
    ) {
        item {
            ScreenTitle(
                title = "Favorite",
                subtitle = "Have a great workout"
            )
            favoriteExercises.forEach { exercise ->
                ExerciseItem(exercise = exercise) { toggledExercise ->
                    toggledExercise.isFavorited = !toggledExercise.isFavorited
                }
            }
        }
    }
    NavigationBar(navController = navController)
}

