package hr.ferit.katicilan.GymFriend

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await


class DataViewModel : ViewModel() {

    private val _exercises = MutableLiveData<List<Exercise>>()
    val exercises: LiveData<List<Exercise>> = _exercises

    init {
        getDataFromFirestore()
    }

    private fun getDataFromFirestore() {
        viewModelScope.launch {
            try {
                val exercisesList = mutableListOf<Exercise>()
                val querySnapshot =
                    FirebaseFirestore.getInstance().collection("exercises").get().await()

                for (document in querySnapshot.documents) {
                    val exercise = document.toObject(Exercise::class.java)
                    exercise?.let {
                        exercisesList.add(it)
                    }
                }
                _exercises.value = exercisesList
            } catch (e: FirebaseFirestoreException) {
                Log.d("error", "RetrieveDataFromFIrebase: $e")
            }
        }
    }
}

